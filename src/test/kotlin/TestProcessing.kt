/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import ch.memobase.App
import ch.memobase.IndexReport
import ch.memobase.KafkaTopology
import kotlinx.serialization.json.Json
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TopologyTestDriver
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File
import java.nio.charset.Charset

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestProcessing {

    private val resourcePath = "src/test/resources"
    private fun readFile(fileName: String): String {
        return File("$resourcePath/$fileName").readText(Charset.defaultCharset())
    }

    @Test
    fun `test processor`() {
        val settings = App.loadSettings("app.yml")
        val testDriver =
            TopologyTestDriver(KafkaTopology(settings).build(), settings.kafkaStreamsSettings)
        val inputTopic =
            testDriver.createInputTopic(settings.inputTopic, StringSerializer(), StringSerializer())
        val outputTopic =
            testDriver.createOutputTopic(settings.outputTopic, StringDeserializer(), StringDeserializer())

        inputTopic.pipeInput("identifier-key", readFile("input.json"))
        val record = outputTopic.readRecord()
        val result = Json.decodeFromString(
            IndexReport.serializer(), record.value)
        assertAll( "",
            { assertThat(result.step).isEqualTo("fedora-ingest") },
            { assertThat(result.message).isEqualTo("Ingested resource https://memobase.ch/record/SRF-Kultur-BS_MG_K_09835.") },
            { assertThat(result.id).isEqualTo("https://memobase.ch/record/SRF-Kultur-BS_MG_K_09835") },
            { assertThat(record.key).isEqualTo("identifier-key-fedora-ingest-PLACEHOLDER") }
        )
    }
}