/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.settings.SettingsLoader
import org.apache.kafka.streams.KafkaStreams

class Service(settings: SettingsLoader) {
    private val stream = KafkaStreams(KafkaTopology(settings).build(), settings.kafkaStreamsSettings)

    init {
        stream.use {
            it.start()
            while (stream.state().isRunningOrRebalancing) {
                Thread.sleep(10_000L)
            }
            throw Exception("Stream stopped running!")
        }
    }
}