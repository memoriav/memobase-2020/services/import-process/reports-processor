/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.settings.SettingsLoader
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.message.ObjectMessage
import kotlin.system.exitProcess

class App {
    companion object {
        const val STEP_VERSION_PROPERTY_NAME = "stepVersion"

        private val log = LogManager.getLogger(this::class.java)
        @JvmStatic fun main(args: Array<String>) {
            try {
                Service(loadSettings("app.yml"))
            } catch (ex: Exception) {
                log.error(ObjectMessage(ex))
                exitProcess(1)
            }
        }

        fun loadSettings(fileName: String): SettingsLoader {
            return SettingsLoader(
                listOf(STEP_VERSION_PROPERTY_NAME),
                fileName,
                useStreamsConfig = true
            )
        }
    }
}
