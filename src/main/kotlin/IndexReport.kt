/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.reporting.ReportStatus
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import java.time.LocalDateTime

@Serializable
class IndexReport(
    val id: String,
    @SerialName("@timestamp")
    val timestamp: String,
    val sessionId: String,
    val recordSetId: String,
    val institutionId: String,
    val isPublished: Boolean,
    val status: String,
    val message: String,
    val step: String,
    val content: String,
    val stepVersion: String,
) {

    @OptIn(ExperimentalSerializationApi::class)
    fun toJson(stepVersion: String): String {
        val format = Json {
            prettyPrint = false
            explicitNulls = false
        }
        return try {
            format.encodeToString(serializer(), this)
        } catch (ex: SerializationException) {
            format.encodeToString(
                serializer(), error(
                    id,
                    "SerializationException: ${ex.localizedMessage}.",
                    "",
                    sessionId,
                    recordSetId,
                    institutionId,
                    isPublished,
                    stepVersion
                )
            )
        }
    }

    companion object {
        fun error(
            key: String, message: String, content: String,
            sessionId: String,
            recordSetId: String,
            institutionId: String,
            isPublished: Boolean,
            stepVersion: String,
        ) = IndexReport(
            timestamp = LocalDateTime.now().toString(),
            sessionId = sessionId,
            recordSetId = recordSetId,
            institutionId = institutionId,
            isPublished = isPublished,
            status = ReportStatus.fatal,
            step = "report-processing",
            message = message,
            content = content,
            id = key,
            stepVersion = stepVersion
        )


    }
}