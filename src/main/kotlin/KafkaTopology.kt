/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.App.Companion.STEP_VERSION_PROPERTY_NAME
import ch.memobase.reporting.Report
import ch.memobase.settings.HeaderExtractionSupplier
import ch.memobase.settings.SettingsLoader
import kotlinx.serialization.SerializationException
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.message.ObjectMessage

class KafkaTopology(
    private val settings: SettingsLoader
) {
    private var count = 0
    private val log = LogManager.getLogger(this::class.java)
    fun build(): Topology {
        val builder = StreamsBuilder()

        val stream = builder.stream<String, String>(settings.inputTopic)
            .processValues(HeaderExtractionSupplier<String>())
            .mapValues { readOnlyKey, value ->
                try {
                    val report = Report.fromJson(value.first)
                    IndexReport(
                        id = report.id,
                        step = report.step,
                        sessionId = value.second.sessionId,
                        recordSetId = value.second.recordSetId,
                        institutionId = value.second.institutionId,
                        isPublished = value.second.isPublished,
                        content = report.content ?: "",
                        message = report.message,
                        status = report.status,
                        timestamp = report.timestamp,
                        stepVersion = report.stepVersion,
                    )
                } catch (ex: SerializationException) {
                    log.error(ObjectMessage(ex))
                    IndexReport.error(
                        readOnlyKey,
                        "SerializationException: ${ex.localizedMessage}.",
                        value.first,
                        value.second.sessionId,
                        value.second.recordSetId,
                        value.second.institutionId,
                        value.second.isPublished,
                        settings.appSettings.getProperty(STEP_VERSION_PROPERTY_NAME)
                    )
                }
            }.map { key, value ->
                KeyValue(
                    "$key-${value.step}-${value.sessionId}", value.toJson(
                        settings.appSettings.getProperty(STEP_VERSION_PROPERTY_NAME)
                    )
                )
            }

        stream.foreach { _, _ ->
            count += 1
            if (count % 10000 == 0) {
                log.info("Processed $count messages since last start.")
            }
        }

        stream.to(settings.outputTopic)
        return builder.build()
    }
}